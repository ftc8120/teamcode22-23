package org.firstinspires.ftc.electrichornets.teleop;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.electrichornets.ShmoveThings;

@TeleOp(name = "Unbreak Lift", group = "Linear OpMode")
public class UnbreakLift extends LinearOpMode {
    private static final ShmoveThings SHMOVE_THINGS = new ShmoveThings();

    @Override
    public void runOpMode(){
        SHMOVE_THINGS.initialize(hardwareMap,false);
        waitForStart();

        while(opModeIsActive()) {
            if(gamepad1.dpad_down){
                SHMOVE_THINGS.goDownPlease();
            } else if(gamepad1.dpad_up){
                SHMOVE_THINGS.goUpPlease();
            } else {
                SHMOVE_THINGS.unshmoveLift();
            }
            if(gamepad1.a){
                SHMOVE_THINGS.setBottomOfLift();
            }
        }
    }
}
