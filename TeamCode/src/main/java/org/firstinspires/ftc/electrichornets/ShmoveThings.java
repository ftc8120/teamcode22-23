package org.firstinspires.ftc.electrichornets;

import androidx.annotation.NonNull;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

public class ShmoveThings {
    DcMotor frontLeft;
    DcMotor frontRight;
    DcMotor backLeft;
    DcMotor backRight;

    DcMotor lift;

    Servo leftClaw;
    Servo rightClaw;
    Servo rotator;


    BNO055IMU imu;
    static boolean weInitedImu = false;
    public void initialize(@NonNull HardwareMap hardwareMap, boolean shouldInitImu) {
        imu = hardwareMap.get(BNO055IMU.class, "imu");
        frontLeft = hardwareMap.get(DcMotor.class, "frontLeft");
        frontRight = hardwareMap.get(DcMotor.class, "frontRight");
        backLeft = hardwareMap.get(DcMotor.class, "backLeft");
        backRight = hardwareMap.get(DcMotor.class, "backRight");
        leftClaw = hardwareMap.get(Servo.class,"leftClaw");
        rightClaw = hardwareMap.get(Servo.class,"rightClaw");
        rotator = hardwareMap.servo.get("rotator");

        lift=hardwareMap.dcMotor.get("lift");

        rightClaw.setDirection(Servo.Direction.REVERSE);

        frontRight.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction
        backRight.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction

        lift.setDirection(DcMotorSimple.Direction.REVERSE); //FLIP MOTOR BUT MAYBE NOT!? FIND OUT NEXT TIME ON DRAGON BALL Z!!!

        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        setBottomOfLift();

        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        if(shouldInitImu || !weInitedImu){
            orient();
            weInitedImu=true;
        }
    }

    public void orient(){
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.calibrationDataFile = "shmoveItShmoveIt.json";
        imu.initialize(parameters);
    }

    public void steadyRotator(){
        rotator.setPosition(0);
    }
    public void shmovement(double angleHeading, double speed, double rightTurnPower) {
        double adjustedAngle = angleHeading - Math.toRadians(45); //Turn the angle 45 degrees to make things normal
        double forwardRightPower = Math.cos(adjustedAngle);
        double forwardLeftPower = Math.sin(adjustedAngle);
        double frontLeftPower = (-((speed * forwardRightPower) + rightTurnPower));//makes wheels move correctly
        double frontRightPower = (-((speed * forwardLeftPower) - rightTurnPower));//makes wheels move correctly
        double backLeftPower = (-((speed * forwardLeftPower) + rightTurnPower));//makes wheels move correctly
        double backRightPower = (-((speed * forwardRightPower) - rightTurnPower));//makes wheels move correctly
        double greater = Math.max(Math.abs(frontRightPower), Math.abs(backLeftPower));
        double high = Math.max(Math.abs(backRightPower), Math.abs(frontLeftPower));
        double powerdivisor = Math.max(greater, high);

        if(powerdivisor <= 1){
            powerdivisor = 1;

        }

        frontRight.setPower(frontRightPower / powerdivisor);
        frontLeft.setPower(frontLeftPower / powerdivisor);
        backLeft.setPower(backLeftPower / powerdivisor);
        backRight.setPower(backRightPower / powerdivisor);


        //Right Turn power controls spinning
        //(speed*forward___Power) controls movement
    }
    public float getHeading() {
        return imu.getAngularOrientation().firstAngle; //COUNTER CLOCKWISE
    }

    public void setBottomOfLift(){
        lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lift.setTargetPosition(0);
        lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void goDownPlease(){
        lift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lift.setPower(-0.1);
    }
    public void goUpPlease(){
        lift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lift.setPower(0.1);
    }

    public void unshmoveLift(){
        lift.setPower(0);
    }

    public void liftTo(int position){
        lift.setTargetPosition(position);
        lift.setPower(1);
    }

    public void closeClaw(){
        leftClaw.setPosition(1);
        rightClaw.setPosition(1);
    }
    public void openClaw(){
        leftClaw.setPosition(0.4);
        rightClaw.setPosition(0.65);
    }

    //Failsafe
    public void liftUpWithoutEncoder(){
        lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        lift.setPower(0.5);
    }

    public void liftDownWithoutEncoder(){
        lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        lift.setPower(-0.5);
    }

    public void shmoveRotatorCenter(){
        rotator.setPosition(1);
    }

    public void shmoveRotatorSide(){
        rotator.setPosition(0);
    }

    public void sitAndStay(){
        frontLeft.setPower(0);
        frontRight.setPower(0);
        backLeft.setPower(0);
        backRight.setPower(0);
    }
}
