package org.firstinspires.ftc.electrichornets.autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;

import org.firstinspires.ftc.electrichornets.ShmoveThings;

@Autonomous(name = "Billy Prime", group = "Robot")
public class DoThingsByYourself extends LinearOpMode {
    private static final ShmoveThings SHMOVE_THINGS = new ShmoveThings();
    public static final double FORWARD = Math.PI / 2;
    public static final double LEFT = Math.PI;
    public static final double RIGHT = 0;
    public static final double BACKWARD = -Math.PI / 2;
    NormalizedColorSensor colorSensorFloor;
    NormalizedColorSensor colorSensorForward;

    public void goLeftPlease() throws InterruptedException {
        SHMOVE_THINGS.shmovement(LEFT, .5, 0);
        Thread.sleep(1000);
        SHMOVE_THINGS.sitAndStay();
    }

    public void runOpMode() throws InterruptedException {
        SHMOVE_THINGS.initialize(hardwareMap,true);
        //colorSensorFloor = hardwareMap.get(NormalizedColorSensor.class, "floorColorSensor");
        colorSensorForward = hardwareMap.get(NormalizedColorSensor.class, "forwardColorSensor");
        waitForStart();

        SHMOVE_THINGS.shmovement(FORWARD, .30, 0); //Drives to cone
        Thread.sleep(1800);
        SHMOVE_THINGS.sitAndStay();
        Thread.sleep(1000);

        float blueForward = colorSensorForward.getNormalizedColors().blue;
        float redForward = colorSensorForward.getNormalizedColors().red;
        float greenForward = colorSensorForward.getNormalizedColors().green;


        if (blueForward > greenForward && blueForward > redForward) {  //BLUE!?!!!!
            telemetry. addData("BLUE", "BLUE");
            SHMOVE_THINGS.shmovement(FORWARD, .50, 0);
            Thread.sleep(500); //THIS IS AUTONOMOUS AND WE CAN BLOCK
            SHMOVE_THINGS.shmovement(RIGHT, .50, 0);
            Thread.sleep(1800); //COMMENTING ON CODE BECAUSE THERE IS A COMMENT HERE, THUS A COMMENT NEEDED TO BE MADE; COMMENTING IS NEEDED FOR ALL GOOD CODE AND THUS THIS CODE IS GOOD AS IT HAS A COMMENT;
            SHMOVE_THINGS.sitAndStay();

        } else if (greenForward > redForward) {  //green :).
            telemetry. addData("GREEN", "GREEN");
            SHMOVE_THINGS.shmovement(FORWARD, .50, 0);
            Thread.sleep(500);//THIS IS AUTONOMOUS AND WE CAN BLOCK
            SHMOVE_THINGS.sitAndStay();
        } else {  //the color red
            telemetry. addData("RED", "RED");
            SHMOVE_THINGS.shmovement(FORWARD, .50, 0);//THIS FUNCTION IS IMPORTANT AS IT MAKES THE ROBOT GO AND ALL ROBOTS NEED TO GO SO THIS FUNCTION IS VERY IMPORTANT TO MAKE THIS ROBOT GO THIS FUNCTION MAKES THE ROBOT GO FORWARD BECAUSE ALL ROBOTS NEED TO GO FORWARD AND THIS FUNCTION LETS THE ROBOT GO FORWARD
            Thread.sleep(500);//THIS IS AUTONOMOUS AND WE CAN BLOCK
            SHMOVE_THINGS.shmovement(LEFT, .50, 0);
            Thread.sleep(1800);//THIS IS AUTONOMOUS AND WE CAN BLOCK
            SHMOVE_THINGS.sitAndStay();
        }

        telemetry.addData("RGB Forward", "{Red, Green, Blue} = %.1f, %.1f, %.1f", redForward, greenForward, blueForward); //WE NEEDED THIS AND SO WE HAVE COMMENTED IT WE DO NOT CARE ABOUT SPACING
        telemetry.update();
        Thread.sleep(5000); //THIS IS AUTONOMOUS AND WE CAN BLOCK
    }
}
