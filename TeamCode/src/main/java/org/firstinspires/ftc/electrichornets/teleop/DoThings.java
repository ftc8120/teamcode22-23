package org.firstinspires.ftc.electrichornets.teleop;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.electrichornets.ShmoveThings;

@TeleOp(name = "MOST OF THE SHMOOOOVES", group = "Linear OpMode")
public class DoThings extends LinearOpMode {
    private static final double HALF_TURN = Math.PI;
    private static final double FULL_TURN = 2 * HALF_TURN;
    private static final int HIGH = 6000; //Change later (Theoretically)
    private static final int MID = 6000;
    private static final int LOW = 3700;
    private static final int GROUND = 750;
    private int targetForLift = HIGH;
    private final ShmoveThings shmoveThings = new ShmoveThings();
    private final ElapsedTime time=new ElapsedTime();
    private boolean triggerHeld=true;
    boolean manualLiftMode =false;

    @Override
    public void runOpMode() {
        shmoveThings.initialize(hardwareMap, false);
        waitForStart();
        while (opModeIsActive()) {
            //DRIVER 2
            shmoveThings.steadyRotator();
            if (gamepad2.a) {
                targetForLift = LOW;
            }

            if (gamepad2.x) {
                targetForLift = GROUND;
            }

            if (gamepad2.b) {
                targetForLift = MID;
            }

            if (gamepad2.y) {
                targetForLift = HIGH;
            }

            if(gamepad2.left_bumper && gamepad2.right_bumper && gamepad2.back){
                manualLiftMode =true;
            }

            //No Encoder movement
            if(manualLiftMode) {
                if (gamepad2.dpad_up) {
                    shmoveThings.liftUpWithoutEncoder();
                } else if (gamepad2.dpad_down) {
                    shmoveThings.liftDownWithoutEncoder();
                } else {
                    shmoveThings.unshmoveLift();
                }
            }

            //DRIVER 1
            //LEFT STICK MOVEMENT

            double leftStickAngle = Math.atan2(-gamepad1.left_stick_y, gamepad1.left_stick_x); //finds the COUNTER CLOCKWISE angle in radians between the stick and the x axis
            double speed = Math.hypot(-gamepad1.left_stick_y, gamepad1.left_stick_x); //distance of stick from (0,0)
            double gyroAngle = leftStickAngle - shmoveThings.getHeading(); //sets the angle ______ to the driver

            //RIGHT STICK MOVEMENT
            double rightTurnPower = 0;
            if (gamepad1.right_stick_x != 0 || gamepad1.right_stick_y != 0) {
                double rightStickAngle = Math.atan2(gamepad1.right_stick_x, -gamepad1.right_stick_y); //CLOCKWISE
                double difOfAngle = rightStickAngle + shmoveThings.getHeading();
                if (difOfAngle > HALF_TURN) {
                    difOfAngle -= FULL_TURN;
                }
                if (difOfAngle < -HALF_TURN) {
                    difOfAngle += FULL_TURN;
                }
                rightTurnPower = calcRightTurn(difOfAngle); //RightTurnPower is set equal to the output of the function calcRightTurn given input "difOfAngle"
            }
            shmoveThings.shmovement(gyroAngle, speed, rightTurnPower);
            telemetry.addData("speed", speed);
            telemetry.update();

            //CLAW AND LIFT
            if (gamepad1.right_trigger != 0) {
                if(!triggerHeld){
                    triggerHeld=true;
                    time.reset();
                }
                shmoveThings.closeClaw();
                if(time.seconds()>1 && !manualLiftMode){
                    shmoveThings.liftTo(targetForLift);
                }
            } else {
                triggerHeld=false;
                shmoveThings.openClaw();
                if(!manualLiftMode) {
                    shmoveThings.liftTo(0);
                }

            }

            if (gamepad1.left_bumper && gamepad1.right_bumper && gamepad1.x){
                shmoveThings.orient();
            }

            /*if (gamepad1.left_trigger != 0) {
                SHMOVE_THINGS.closeClaw();
            } else {
                SHMOVE_THINGS.openClaw();
            }*/
        }
    }

    public double calcRightTurn(double difOfAngle) {

        double power = difOfAngle / Math.PI;
        if (power <= .0125 && power >= -.0125) {
            return 0;
        }

        return (difOfAngle / Math.PI) * 1.5;
    }


}
